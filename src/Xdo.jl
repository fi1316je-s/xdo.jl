module Xdo

export move_mouse, move_mouse_relative, mouse_click, mouse_click_multiple, mouse_up, mouse_down,
       keys_click, keys_up, keys_down, mouse_location


const LIB_XDO = "libxdo.so"

struct CharcodeMap
    key::Cwchar_t
    code::Cuchar
    symbol::Culong
    group::Cint
    modmask::Cint
    needs_binding::Cint
end

struct _XdoCtx
    xdpy::Ptr{Cvoid}
    display_name::Cstring
    charcodes::Ref{CharcodeMap};
    charcodes_len::Cint;
    keycode_high::Cint
    keycode_low::Cint
    keysyms_per_keycode::Cint
    close_display_when_freed::Cint
    quiet::Cint
    debug::Cint
    features_mask::Cint
end

mutable struct XdoCtx
    ptr::Ptr{_XdoCtx}
end

xdo_new() = @ccall LIB_XDO.xdo_new(C_NULL::Cstring)::Ptr{_XdoCtx}
xdo_free(xdo::XdoCtx) = @ccall LIB_XDO.xdo_free(xdo::Ptr{_XdoCtx})::Cvoid
xdo_move_mouse(xdo::XdoCtx, x::Int, y::Int) =
    @ccall LIB_XDO.xdo_move_mouse(xdo::Ptr{_XdoCtx}, x::Cint, y::Cint, 0::Cint)::Cint
xdo_move_mouse_relative(xdo::XdoCtx, x::Int, y::Int) =
    @ccall LIB_XDO.xdo_move_mouse_relative(xdo::Ptr{_XdoCtx}, x::Cint, y::Cint)::Cint
xdo_mouse_down(xdo::XdoCtx, button::Int) =
    @ccall LIB_XDO.xdo_mouse_down(xdo::Ptr{_XdoCtx}, 0::Cint, button::Cint)::Cint
xdo_mouse_up(xdo::XdoCtx, button::Int) =
    @ccall LIB_XDO.xdo_mouse_up(xdo::Ptr{_XdoCtx}, 0::Cint, button::Cint)::Cint
xdo_mouse_click(xdo::XdoCtx, button::Int) =
    @ccall LIB_XDO.xdo_click_window(xdo::Ptr{_XdoCtx}, 0::Cint, button::Cint)::Cint
xdo_mouse_click_multiple(xdo::XdoCtx, button::Int, repeat::Int, μs_delay::UInt32=UInt32(0)) =
    @ccall LIB_XDO.xdo_click_window_multiple(xdo::Ptr{_XdoCtx}, 0::Cint, button::Cint, repeat::Cint, μs_delay::Cuint)::Cint

xdo_send_keys(xdo::XdoCtx, keys::String, μs_delay::UInt32=UInt32(0)) =
    @ccall LIB_XDO.xdo_send_keysequence_window(xdo::Ptr{_XdoCtx}, 0::Cint, keys::Cstring, μs_delay::Cuint)::Cint
xdo_send_keys_up(xdo::XdoCtx, keys::String, μs_delay::UInt32=UInt32(0)) =
    @ccall LIB_XDO.xdo_send_keysequence_window_up(xdo::Ptr{_XdoCtx}, 0::Cint, keys::Cstring, μs_delay::Cuint)::Cint
xdo_send_keys_down(xdo::XdoCtx, keys::String, μs_delay::UInt32=UInt32(0)) =
    @ccall LIB_XDO.xdo_send_keysequence_window_down(xdo::Ptr{_XdoCtx}, 0::Cint, keys::Cstring, μs_delay::Cuint)::Cint

function xdo_mouse_location(xdo::XdoCtx)
    x = Ref{Cint}(0)
    y = Ref{Cint}(0)
    screen_num = Ref{Cint}(0)
    @ccall LIB_XDO."xdo_get_mouse_location"(xdo::Ptr{_XdoCtx}, x::Ref{Cint}, y::Ref{Cint}, screen_num::Ref{Cint})::Cint
    return x[],y[]
end

Base.unsafe_convert(::Type{Ptr{_XdoCtx}}, xdo::XdoCtx) = xdo.ptr

function init_xdo(xdo::XdoCtx)
    xdo.ptr = xdo_new()
    finalizer(xdo_free, xdo)
    return xdo
end

const XDO_CTX = XdoCtx(C_NULL)

move_mouse(x::Int, y::Int) = xdo_move_mouse(XDO_CTX, x, y)
move_mouse_relative(x::Int, y::Int) = xdo_move_mouse_relative(XDO_CTX, x, y)
mouse_down(button::Int) = xdo_mouse_down(XDO_CTX, button)
mouse_up(button::Int) = xdo_mouse_up(XDO_CTX, button)
mouse_click(button::Int) = xdo_mouse_click(XDO_CTX, button)
mouse_click_multiple(button::Int, repeat::Int, μs_delay::UInt32=UInt32(0)) =
    xdo_mouse_click_multiple(XDO_CTX, button, repeat,μs_delay)

keys_click(keys::String, μs_delay::UInt32=UInt32(0)) = xdo_send_keys(XDO_CTX, keys, μs_delay)
keys_up(keys::String, μs_delay::UInt32=UInt32(0)) = xdo_send_keys_up(XDO_CTX, keys, μs_delay)
keys_down(keys::String, μs_delay::UInt32=UInt32(0)) = xdo_send_keys_down(XDO_CTX, keys, μs_delay)

mouse_location() = xdo_mouse_location(XDO_CTX)

function __init__()
    init_xdo(XDO_CTX)
end

end
